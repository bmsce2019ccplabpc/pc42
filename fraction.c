#include<stdio.h>
struct fraction
{
   int num;
   int den;
};
typedef struct fraction Fraction;
Fraction input()
{
   Fraction f;
   printf("enter a fraction \n");
   scanf("%d/%d",&f.num,&f.den);
   return f;
}
Fraction add( Fraction f1, Fraction f2)
{
    Fraction sum;
    sum.num=f1.num*f2.den+f2.num*f1.den;
    sum.den=f1.den*f2.den;
    return sum;
}
void output(Fraction f1,Fraction f2,Fraction sum)
{
   printf("the fraction of %d/%d and %d/%d is %d/%d",f1.num,f1.den,f2.num,f2.den,sum.num,sum.den);
}
int main()
{
   Fraction f1,f2;
   Fraction sum;
   f1=input();
   f2=input();
   sum=add(f1,f2);
   output(f1,f2,sum);
   return 0;
}